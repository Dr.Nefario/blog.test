 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'User'],function()
{
  Route::get('/','HomeController@index');
  // Route::get('/', function () {
   //     return view('user/blog');
  // });
  Route::get('post','PostController@index') ->name('post');
  // Route::get('post', function () {
  //     return view('user/post');
  //    } ) ->name('post');
});


Route::group(['namespace' => 'Admin'],function()
{
  Route::get('admin/home','HomeController@index')-> name('admin.home');
  Route::resource('admin/post','PostController');
  // Route::get('admin/post', function () {
  //     return view('admin.post.post');
  // });

  // User Route

  Route::resource('admin/user','UserController');



  Route::resource('admin/tag','TagController');
  //
  // Route::get('admin/tag', function () {
  //     return view('admin.tag.tag');
  // });

  Route::resource('admin/category','CategoryController');
  // Route::get('admin/category', function () {
  //     return view('admin.category.category');
  // });

});
